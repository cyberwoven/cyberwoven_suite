#Claro Tweaks

Provides admin adjustments to the Claro theme for Cyberwoven projects.

**Functionality:**

* **Admin Pages:** Makes the content edit page more useable by widening the main column and stacking the sidebar, where appropriate. Reduces default font size a bit, and adjust default colors and buttons.
