<?php

namespace Drupal\run_cron\Controller;

use Drupal;
use Drupal\Core\Controller\ControllerBase;

/**
 * Class RunCronController
 *
 * @package Drupal\run_cron\Controller
 */
class RunCronController extends ControllerBase
{
  function runCron()
  {
    Drupal::service('cron')->run();
    return $this->redirect('<front>');
  }
}
