(function ($, Drupal, cookies) {
  Drupal.behaviors.AlertBar = {
    attach: function (context, settings) {

      var cookieintro = 'alert_bar_closed_';
      var alert_bar_closed = cookies.get(cookieintro.concat(settings.cw_alert_bar.unique_id), Number);
      var is_root = (location.pathname === "/");
      if (drupalSettings.alertSettings.homepage_only === 1 && is_root !== true) {
        alert_bar_closed = 1;
      }

      if (!alert_bar_closed) {
        if (drupalSettings.alertSettings.enabled) {
          let currentTime = Math.round(new Date().getTime() / 1000);
          if ((drupalSettings.alertSettings.activates === null || currentTime >= drupalSettings.alertSettings.activates) &&
            (drupalSettings.alertSettings.expires === null || currentTime < drupalSettings.alertSettings.expires)) {
            $('#cw-alert-bar').addClass('show-alert');
          }
        }
      }

      $('.alert-bar-close', context).on('click', function (e) {
        e.preventDefault();
        if ($('#cw-alert-bar').length > 0) {
          $('#cw-alert-bar').removeClass('show-alert');
          cookies.set(cookieintro.concat(settings.cw_alert_bar.unique_id), 1);
        }
      });

    }
  };
})(jQuery, Drupal, window.Cookies);
