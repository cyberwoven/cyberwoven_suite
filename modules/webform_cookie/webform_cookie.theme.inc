<?php

/**
 * @file
 * Theme and preprocess functions for webform_cookie module.
 */

/**
 * Implements hook_preporcess_webform_handler_cookie_summary().
 */
function webform_cookie_preprocess_webform_handler_cookie_summary(array &$variables) {
  $settings = $variables['settings'];

  $variables['name'] = trim($settings['name']);
  $variables['value'] = $settings['value'];
  $variables['duration'] = $settings['expires_duration'] > 0 ? $settings['expires_duration'] . ' ' . $settings['expires_unit'] : t('Session');
  $variables['domain'] = $settings['domain'] ?: \Drupal::request()->getHost();
  $variables['secure'] = $settings['secure'] ? t('Yes') : t('No');
  $variables['http_only'] = $settings['http_only'] ? t('Yes') : t('No');
}
