# Custom Route Permissions

This module provides custom permissions to allow for granting permissions
to existing routes.

## Functionality
There is no settings page for this module. When installed, it creates
permissions available on the site's Permissions page.

## Available Permissions
The following permissions are available through this module under the
"Custom Route Permissions" section of the site's Permissions page:  

*  **Administer Basic site settings:** When checked, this will grant users
of a role access to the **Configuration > System > Basic site settings**
page. Previously, the user needed to have "Administer site configuration"
access which grants them several other unnecessary, and potentially
dangerous permissions. 
