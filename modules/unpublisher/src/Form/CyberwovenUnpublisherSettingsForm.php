<?php

namespace Drupal\unpublisher\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Class UnpublisherSettingsForm.
 */
class UnpublisherSettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * UnpublisherSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManager $entity_type_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'unpublisher.settings'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unpublisher_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Min seconds between cron runs. Default 6 hours (21600 seconds).
    $default_min_time_between_runs = (60 * 60  * 6);

    $config = $this->config('unpublisher.settings');

    $form['description'] = [
      '#type' => 'markup',
      '#markup' => '<p>This module is triggered by cron, and for each content type, will loop through all
          <br/>published content of that type and use the token value you set below to determine whether it should be unpublished.
          <br/>Content will be unpublished some time (depending on when cron is run) after <em>midnight</em> of the date specified.
          <br/>The token should resolve to a date.<br/>
          Eg: <em>[node:field_date_range:end_value]</em> or <em>[node:field_date:value]</em></p>',
      '#weight' => '0',
    ];

    if (!empty($config->get('min_time_between_runs'))) {
      $default_min_time_between_runs = $config->get('min_time_between_runs');
    }

    $form['settings'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE
    );

    $form['settings']['min_time_between_runs'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Run Delay'),
      '#description' => $this->t('The amount of time to delay between cron runs (in seconds)'),
      '#default_value' => $default_min_time_between_runs,
      '#required' => TRUE,
    ];

    $form['types'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Content Types'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE
    );

    // Get all content types.
    $node_types = $this->entityTypeManager
      ->getStorage('node_type')
      ->loadMultiple();

    foreach ($node_types as $node_type) {
      $node_type_id = $node_type->id();
      $node_type_label = $node_type->label();

      // Create a text field for each content type.
      $form['types']['unpublisher_' . $node_type_id . '_unpublish_date'] = [
        '#type' => 'textfield',
        '#title' => $this->t($node_type_label),
        '#required' => FALSE,
        '#default_value' => $config->get($node_type_id . '_unpublish_date'),
      ];
    }

    // Add "Browse available tokens".
    $form['token_browser'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['node'],
      '#click_insert' => FALSE,
      '#dialog' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $min_time_between_runs = $form_state->getValue('min_time_between_runs');

    if (!ctype_digit($min_time_between_runs)) {
      $form_state->setErrorByName($min_time_between_runs, t('Time must be an int >= 0'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('unpublisher.settings');

    $config->set('min_time_between_runs', $form_state->getValue('min_time_between_runs'));

    $node_types = $this->entityTypeManager
      ->getStorage('node_type')
      ->loadMultiple();

    foreach ($node_types as $node_type) {
      $node_type_id = $node_type->id();
      $val = $form_state->getValue('unpublisher_' . $node_type_id . '_unpublish_date');
      $val = trim($val);
      $config->set($node_type_id . '_unpublish_date', $val);
    }

    $config->save();
  }
}
