# Overview:

This module is triggered by cron, and for each content type, will loop through all
published content of that type and use the token value you set in the admin (`admin/content/unpublisher-settings`) to
determine whether it should be unpublished.

## Notes:
Content will be unpublished some time (depending on when cron is run) after midnight of the date specified.
The token should resolve to a date.
