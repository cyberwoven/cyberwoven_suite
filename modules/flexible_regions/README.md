#Flexible Regions

This module allows for regions to be used outside of page templates. To add a region to
an included template (twig) use the `{{ region.region_name }}` syntax. The 
`{{ page.region_name }}` syntax is still reserved for page templates.

NOTE: This module has been deprecated, and is only included for backward compatibility.
If you require this functionality, use Twig Tweak instead.
