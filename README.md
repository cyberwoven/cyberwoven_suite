# Cyberwoven Suite
This modules provides Cyberwoven-specific configuration tweaks, and a collection of sub-modules to
improve the usability and functionality of Drupal for Cyberwoven websites.

## Configuration Tweaks

**Webform Tweaks:**

  Forcibly set each webform's "Allow users to post submissions from a dedicated URL"
  configuration setting to **false** on webform entity presave. This will prevent the
  webform from being submitted unless its embedded/attached to another entity as a
  block. This setting is found on each Webform under **Settings -> General -> URL path
  settings**.


**Block Tweaks:**

  * Adds an additional tab to block configurations to allow the block to be placed on
    all pages that are not nodes.
  * Adds a "Negate" option to the content type restrictions settings.

**Text Formats and Editors Config:**

  The following three configuration files are included under _config/install_:

  * linkit.linkit_profile.cyberwoven_standard.yml
  * editor.editor.basic_html.yml
  * editor.editor.full_html.yml
  
  These configurations create the "Cyberwoven Standard" Linkit profile, enable the 
  Linkit style link buttons in the Active Toolbars (for the Full and Basic text formats),
  and activates the "Cyberwoven Standard" profile for each.

## Included Sub-modules
* **Alert Bar (modules/alert_bar):** This modules provides a simple themeable alert bar.
* **Content Modification Log (modules/content_modification_log)** This module implements a
  global content modification log. It begins tracking modifications after after it is 
  enabled, and will display basic information about the modified entity, the user performing
  the modification, and the timestamp the modification was made.
* **Custom CSS Editor (modules/custom_css_editor):** This module provides a field on the node
  edit form to inject custom CSS. **Note:** This module requires a twig variable to be added
  to the bottom of the `<head>` section of the `html.html.twig` template file.
* **Custom Route Permissions (modules/custom_route_permissions):** This module provides custom
  permissions to allow for granting permissions to existing routes.
* **Extended Theme Suggestions (modules/extended_theme_suggestions):** This module provides a
  few theme suggestions that we tend to want during themeing of Drupal sites.
* **Flexible Regions (modules/flexible_regions):** This module allows for regions to be used
  outside of page templates.
* **Unpublisher (modules/unpublisher):** This module uses scheduled cron runs to unpublish
  content based on token values set in the admin
* **Webform Cookie (module/webform/cookie):** This module creates a Webform submission handler
  that sets a cookie on a webform submission.
