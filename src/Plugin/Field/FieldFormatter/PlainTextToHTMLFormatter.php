<?php
namespace Drupal\cyberwoven_suite\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'plain_text_to_html' formatter.
 *
 * @FieldFormatter(
 *   id = "plain_text_to_html",
 *   label = @Translation("Plain text to HTML"),
 *   field_types = {
 *     "string",
 *     "string_long",
 *   },
 * )
 */
class PlainTextToHTMLFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'inline_template',
        '#template' => '{{ value|raw }}',
        '#context' => ['value' => $item->value],
      ];
    }

    return $elements;
  }
}
