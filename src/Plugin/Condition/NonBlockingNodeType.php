<?php

namespace Drupal\cyberwoven_suite\Plugin\Condition;

use Drupal\node\NodeInterface;
use Drupal\Core\Entity\Plugin\Condition\EntityBundle;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Node Type' condition to doesn't block on non-nodes
 *
 * @Condition(
 *   id = "non_blocking_node_type",
 *   label = @Translation("Content types"),
 *   context = { }
 * )
 */
class NonBlockingNodeType extends EntityBundle {

  /**
   * Current route matcher.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * Creates a new EntityBundle instance.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   The current route match interface.
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info service.
   */
  public function __construct(RouteMatchInterface $current_route_match, array $configuration, $plugin_id, $plugin_definition, EntityTypeBundleInfoInterface $entity_type_bundle_info) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_bundle_info);
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('current_route_match'),
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeId() {
    // Could set the derivative via the plugin id (non_blocking_node_type:node) and not have to override this function,
    // but that would break sites that are already using this condition. Would have to manually export the block config,
    // update the id where it is referenced, and re-import. So just gonna hard code this.
    return 'node';
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $bundles = $this->entityTypeBundleInfo->getBundleInfo($this->getDerivativeId());

    $form['#attached']['library'][] = 'cyberwoven_suite/cyberwoven_suite.block';

    $form['bundles'] = [
      '#title' => $this->pluginDefinition['label'],
      '#type' => 'checkboxes',
      '#options' => array_combine(array_keys($bundles), array_column($bundles, 'label')),
      '#weight' => -2,
      '#default_value' => $this->configuration['bundles'],
    ];

    $form['all_others'] = [
      '#title' => $this->t('All non-node pages'),
      '#type' => 'checkbox',
      '#weight' => -1,
      '#default_value' => ((isset($this->configuration['all_others'])) ? $this->configuration['all_others'] : NULL),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['bundles'] = array_filter($form_state->getValue('bundles'));
    $this->configuration['all_others'] = $form_state->getValue('all_others');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {

    $others = '';
    if (!empty($this->configuration['all_others'])) {
      $others = $this->t('and all other pages.');
    }

    if (count($this->configuration['bundles']) > 1) {
      $bundles = $this->configuration['bundles'];
      $last = array_pop($bundles);
      $bundles = implode(', ', $bundles);

      if (empty($this->configuration['negate'])) {
        return $this->t('@bundle_type is @bundles or @last @others', [
          '@bundle_type' => $this->pluginDefinition['label'],
          '@bundles' => $bundles,
          '@last' => $last,
          '@others' => $others,
        ]);
      }
      else {
        return $this->t('@bundle_type is not @bundles or @last @others', [
          '@bundle_type' => $this->pluginDefinition['label'],
          '@bundles' => $bundles,
          '@last' => $last,
          '@others' => $others,
        ]);
      }
    }
    $bundle = reset($this->configuration['bundles']);

    if (empty($this->configuration['negate'])) {
      return $this->t('@bundle_type is @bundle @others', [
        '@bundle_type' => $this->pluginDefinition['label'],
        '@bundle' => $bundle,
        '@others' => $others,
      ]);
    }
    else {
      return $this->t('@bundle_type is not @bundle @others', [
        '@bundle_type' => $this->pluginDefinition['label'],
        '@bundle' => $bundle,
        '@others' => $others,
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {

    // Returns true if no bundles are selected and negate option is disabled.
    if (empty($this->configuration['bundles']) && !$this->configuration['all_others'] && !$this->isNegated()) {
      return TRUE;
    }

    $routeName = $this->currentRouteMatch->getRouteName();

    if ($this->configuration['all_others'] && !in_array($routeName, ['entity.node.canonical', 'entity.node.revision'])) {
      return TRUE;
    }

    if (in_array($routeName, ['entity.node.canonical', 'entity.node.revision'])) {

      $node = $this->currentRouteMatch->getParameter('node');
      if (is_scalar($node)) {
        $node = \Drupal::entityTypeManager()->getStorage('node')->load($node);
      }

      if ($node instanceof NodeInterface) {
        return !empty($this->configuration['bundles'][$node->getType()]);
      }
    }
  }
}
